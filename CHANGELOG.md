# [1.1.0](https://gitlab.com/stevencheng71/202402-automate-release-win/compare/v1.0.0...v1.1.0) (2024-2-24)


### Features

* add xxx ([c143f43](https://gitlab.com/stevencheng71/202402-automate-release-win/commit/c143f430d3720b8be58d6cdfe08a8c796b7363de))

# 1.0.0 (2024-02-24)


### Features

* add husky and commitlint ([d81426a](https://gitlab.com/stevencheng71/202402-automate-release-win/commit/d81426aa429c798df77f59aa9c57cb59c30764fb))
